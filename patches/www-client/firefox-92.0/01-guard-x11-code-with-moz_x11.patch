diff --git a/gfx/gl/GLContextProviderWayland.cpp b/gfx/gl/GLContextProviderWayland.cpp
index 3f1f926..79a365e 100644
--- a/gfx/gl/GLContextProviderWayland.cpp
+++ b/gfx/gl/GLContextProviderWayland.cpp
@@ -14,7 +14,9 @@ namespace mozilla::gl {
 using namespace mozilla::gfx;
 using namespace mozilla::widget;
 
+#ifdef MOZ_X11
 static class GLContextProviderX11 sGLContextProviderX11;
+#endif
 static class GLContextProviderEGL sGLContextProviderEGL;
 
 // Note that if there is no GTK display, `GdkIsX11Display` and
@@ -34,8 +36,10 @@ already_AddRefed<GLContext> GLContextProviderWayland::CreateForCompositorWidget(
     return sGLContextProviderEGL.CreateForCompositorWidget(
         aCompositorWidget, aHardwareWebRender, aForceAccelerated);
   } else {
+#ifdef MOZ_X11
     return sGLContextProviderX11.CreateForCompositorWidget(
         aCompositorWidget, aHardwareWebRender, aForceAccelerated);
+#endif
   }
 }
 
@@ -45,7 +49,9 @@ already_AddRefed<GLContext> GLContextProviderWayland::CreateHeadless(
   if (GdkIsWaylandDisplay()) {
     return sGLContextProviderEGL.CreateHeadless(desc, out_failureId);
   } else {
+#ifdef MOZ_X11
     return sGLContextProviderX11.CreateHeadless(desc, out_failureId);
+#endif
   }
 }
 
@@ -54,7 +60,9 @@ GLContext* GLContextProviderWayland::GetGlobalContext() {
   if (GdkIsWaylandDisplay()) {
     return sGLContextProviderEGL.GetGlobalContext();
   } else {
+#ifdef MOZ_X11
     return sGLContextProviderX11.GetGlobalContext();
+#endif
   }
 }
 
@@ -63,7 +71,9 @@ void GLContextProviderWayland::Shutdown() {
   if (GdkIsWaylandDisplay()) {
     sGLContextProviderEGL.Shutdown();
   } else {
+#ifdef MOZ_X11
     sGLContextProviderX11.Shutdown();
+#endif
   }
 }
 
diff --git a/gfx/layers/ipc/CompositorBridgeParent.cpp b/gfx/layers/ipc/CompositorBridgeParent.cpp
index e205967..c2cccd7 100644
--- a/gfx/layers/ipc/CompositorBridgeParent.cpp
+++ b/gfx/layers/ipc/CompositorBridgeParent.cpp
@@ -72,7 +72,7 @@
 #include "mozilla/ProfilerLabels.h"
 #include "mozilla/ProfilerMarkers.h"
 #include "mozilla/Telemetry.h"
-#ifdef MOZ_WIDGET_GTK
+#if defined(MOZ_WIDGET_GTK) && defined(MOZ_X11)
 #  include "basic/X11BasicCompositor.h"  // for X11BasicCompositor
 #endif
 #include "nsCOMPtr.h"         // for already_AddRefed
@@ -1402,7 +1402,7 @@ RefPtr<Compositor> CompositorBridgeParent::NewCompositor(
           new CompositorOGL(this, mWidget, mEGLSurfaceSize.width,
                             mEGLSurfaceSize.height, mUseExternalSurfaceSize);
     } else if (aBackendHints[i] == LayersBackend::LAYERS_BASIC) {
-#ifdef MOZ_WIDGET_GTK
+#if defined(MOZ_WIDGET_GTK) && defined(MOZ_X11)
       if (gfxVars::UseXRender()) {
         compositor = new X11BasicCompositor(this, mWidget);
       } else
diff --git a/gfx/thebes/gfxPlatformGtk.cpp b/gfx/thebes/gfxPlatformGtk.cpp
index ff09228..bde211b 100644
--- a/gfx/thebes/gfxPlatformGtk.cpp
+++ b/gfx/thebes/gfxPlatformGtk.cpp
@@ -41,6 +41,7 @@
 #include "nsUnicharUtils.h"
 #include "nsUnicodeProperties.h"
 #include "VsyncSource.h"
+#include "mozilla/WidgetUtilsGtk.h"
 
 #ifdef MOZ_X11
 #  include <gdk/gdkx.h>
diff --git a/gfx/webrender_bindings/RenderCompositorOGLSWGL.cpp b/gfx/webrender_bindings/RenderCompositorOGLSWGL.cpp
index 7421012..a336fad 100644
--- a/gfx/webrender_bindings/RenderCompositorOGLSWGL.cpp
+++ b/gfx/webrender_bindings/RenderCompositorOGLSWGL.cpp
@@ -27,7 +27,9 @@
 #ifdef MOZ_WIDGET_GTK
 #  include "mozilla/widget/GtkCompositorWidget.h"
 #  include <gdk/gdk.h>
-#  include <gdk/gdkx.h>
+#  ifdef MOZ_X11
+#    include <gdk/gdkx.h>
+#  endif
 #endif
 
 namespace mozilla {
diff --git a/toolkit/components/remote/nsRemoteService.cpp b/toolkit/components/remote/nsRemoteService.cpp
index 289fa50..5224462 100644
--- a/toolkit/components/remote/nsRemoteService.cpp
+++ b/toolkit/components/remote/nsRemoteService.cpp
@@ -12,8 +12,10 @@
 
 #ifdef MOZ_WIDGET_GTK
 #  include "mozilla/WidgetUtilsGtk.h"
-#  include "nsGTKRemoteServer.h"
-#  include "nsXRemoteClient.h"
+#  ifdef MOZ_X11
+#    include "nsGTKRemoteServer.h"
+#    include "nsXRemoteClient.h"
+#  endif
 #  ifdef MOZ_ENABLE_DBUS
 #    include "nsDBusRemoteServer.h"
 #    include "nsDBusRemoteClient.h"
@@ -111,9 +113,11 @@ RemoteResult nsRemoteService::StartClient(const char* aDesktopStartupID) {
     client = MakeUnique<nsDBusRemoteClient>();
   }
 #  endif
+#  ifdef MOZ_X11
   if (!client && useX11Remote) {
     client = MakeUnique<nsXRemoteClient>();
   }
+#  endif
 #elif defined(XP_WIN)
   client = MakeUnique<nsWinRemoteClient>();
 #elif defined(XP_DARWIN)
@@ -160,9 +164,11 @@ void nsRemoteService::StartupServer() {
     mRemoteServer = MakeUnique<nsDBusRemoteServer>();
   }
 #  endif
+#  ifdef MOZ_X11
   if (!mRemoteServer && useX11Remote) {
     mRemoteServer = MakeUnique<nsGTKRemoteServer>();
   }
+#  endif
 #elif defined(XP_WIN)
   mRemoteServer = MakeUnique<nsWinRemoteServer>();
 #elif defined(XP_DARWIN)
diff --git a/toolkit/xre/nsGDKErrorHandler.cpp b/toolkit/xre/nsGDKErrorHandler.cpp
index 421abdf..0d00884 100644
--- a/toolkit/xre/nsGDKErrorHandler.cpp
+++ b/toolkit/xre/nsGDKErrorHandler.cpp
@@ -6,14 +6,18 @@
 #include "nsGDKErrorHandler.h"
 
 #include <gtk/gtk.h>
-#include <gdk/gdkx.h>
+#ifdef MOZ_X11
+#  include <gdk/gdkx.h>
+#endif
 #include <errno.h>
 #include <stdlib.h>
 #include <string.h>
 
 #include "nsDebug.h"
 #include "nsString.h"
-#include "nsX11ErrorHandler.h"
+#ifdef MOZ_X11
+#  include "nsX11ErrorHandler.h"
+#endif
 
 #include "prenv.h"
 
@@ -26,6 +30,7 @@
  */
 static void GdkErrorHandler(const gchar* log_domain, GLogLevelFlags log_level,
                             const gchar* message, gpointer user_data) {
+#ifdef MOZ_X11
   if (strstr(message, "X Window System error")) {
     XErrorEvent event;
     nsDependentCString buffer(message);
@@ -92,7 +97,9 @@ static void GdkErrorHandler(const gchar* log_domain, GLogLevelFlags log_level,
     event.resourceid = 0;
 
     X11Error(event.display, &event);
-  } else {
+  } else
+#endif
+  {
     g_log_default_handler(log_domain, log_level, message, user_data);
     MOZ_CRASH_UNSAFE(message);
   }
@@ -103,7 +110,9 @@ void InstallGdkErrorHandler() {
                     (GLogLevelFlags)(G_LOG_LEVEL_ERROR | G_LOG_FLAG_FATAL |
                                      G_LOG_FLAG_RECURSION),
                     GdkErrorHandler, nullptr);
+#ifdef MOZ_X11
   if (PR_GetEnv("MOZ_X_SYNC")) {
     XSynchronize(GDK_DISPLAY_XDISPLAY(gdk_display_get_default()), X11True);
   }
+#endif
 }
diff --git a/widget/gtk/WindowSurfaceWayland.cpp b/widget/gtk/WindowSurfaceWayland.cpp
index 036555c..182a485 100644
--- a/widget/gtk/WindowSurfaceWayland.cpp
+++ b/widget/gtk/WindowSurfaceWayland.cpp
@@ -9,6 +9,7 @@
 #include <errno.h>
 #include <fcntl.h>
 #include <sys/mman.h>
+#include <prenv.h>
 
 #include "nsPrintfCString.h"
 #include "mozilla/gfx/2D.h"
diff --git a/widget/gtk/mozgtk/mozgtk.c b/widget/gtk/mozgtk/mozgtk.c
index 677f9b3..d7dbbc6 100644
--- a/widget/gtk/mozgtk/mozgtk.c
+++ b/widget/gtk/mozgtk/mozgtk.c
@@ -6,7 +6,8 @@
 
 #include "mozilla/Types.h"
 
-#include <X11/Xlib.h>
+#ifdef MOZ_X11
+#  include <X11/Xlib.h>
 // Bug 1271100
 // We need to trick system Cairo into not using the XShm extension due to
 // a race condition in it that results in frequent BadAccess errors. Cairo
@@ -19,3 +20,4 @@
 // ever can remove this workaround for system Cairo, we'll need something
 // to replace it for that purpose.
 MOZ_EXPORT Bool XShmQueryExtension(Display* aDisplay) { return False; }
+#endif
diff --git a/widget/gtk/nsClipboard.cpp b/widget/gtk/nsClipboard.cpp
index ae7eadd..1e58aee 100644
--- a/widget/gtk/nsClipboard.cpp
+++ b/widget/gtk/nsClipboard.cpp
@@ -96,7 +96,9 @@ NS_IMPL_ISUPPORTS(nsClipboard, nsIClipboard, nsIObserver)
 
 nsresult nsClipboard::Init(void) {
   if (widget::GdkIsX11Display()) {
+#if defined(MOZ_X11)
     mContext = MakeUnique<nsRetrievalContextX11>();
+#endif
 #if defined(MOZ_WAYLAND)
   } else if (widget::GdkIsWaylandDisplay()) {
     mContext = MakeUnique<nsRetrievalContextWayland>();
diff --git a/widget/gtk/nsClipboardX11.cpp b/widget/gtk/nsClipboardX11.cpp
index 652b239..d5cac5b 100644
--- a/widget/gtk/nsClipboardX11.cpp
+++ b/widget/gtk/nsClipboardX11.cpp
@@ -23,14 +23,16 @@
 #include <gtk/gtk.h>
 
 // For manipulation of the X event queue
-#include <X11/Xlib.h>
-#include <poll.h>
-#include <gdk/gdkx.h>
-#include <sys/time.h>
-#include <sys/types.h>
-#include <errno.h>
-#include <unistd.h>
-#include "X11UndefineNone.h"
+#ifdef MOZ_X11
+#  include <X11/Xlib.h>
+#  include <poll.h>
+#  include <gdk/gdkx.h>
+#  include <sys/time.h>
+#  include <sys/types.h>
+#  include <errno.h>
+#  include <unistd.h>
+#  include "X11UndefineNone.h"
+#endif
 
 using namespace mozilla;
 
@@ -46,6 +48,7 @@ nsRetrievalContextX11::nsRetrievalContextX11()
       mClipboardDataLength(0),
       mTargetMIMEType(gdk_atom_intern("TARGETS", FALSE)) {}
 
+#ifdef MOZ_X11
 static void DispatchSelectionNotifyEvent(GtkWidget* widget, XEvent* xevent) {
   GdkEvent event = {};
   event.selection.type = GDK_SELECTION_NOTIFY;
@@ -98,12 +101,14 @@ static Bool checkEventProc(Display* display, XEvent* event, XPointer arg) {
 
   return X11False;
 }
+#endif
 
 bool nsRetrievalContextX11::WaitForX11Content() {
   if (mState == COMPLETED) {  // the request completed synchronously
     return true;
   }
 
+#ifdef MOZ_X11
   GdkDisplay* gdkDisplay = gdk_display_get_default();
   // gdk_display_get_default() returns null on headless
   if (mozilla::widget::GdkIsX11Display(gdkDisplay)) {
@@ -146,6 +151,7 @@ bool nsRetrievalContextX11::WaitForX11Content() {
     } while ((poll_result == 1 && (pfd.revents & (POLLHUP | POLLERR)) == 0) ||
              (poll_result == -1 && errno == EINTR));
   }
+#endif
 #ifdef DEBUG_CLIPBOARD
   printf("exceeded clipboard timeout\n");
 #endif
diff --git a/widget/gtk/nsDragService.cpp b/widget/gtk/nsDragService.cpp
index 82d6d25..eb92b2d 100644
--- a/widget/gtk/nsDragService.cpp
+++ b/widget/gtk/nsDragService.cpp
@@ -33,7 +33,9 @@
 #include "mozilla/WidgetUtilsGtk.h"
 #include "GRefPtr.h"
 
-#include "gfxXlibSurface.h"
+#ifdef MOZ_X11
+#  include "gfxXlibSurface.h"
+#endif
 #include "gfxContext.h"
 #include "nsImageToPixbuf.h"
 #include "nsPresContext.h"
diff --git a/widget/gtk/nsGtkKeyUtils.cpp b/widget/gtk/nsGtkKeyUtils.cpp
index f4c7030..c58a777 100644
--- a/widget/gtk/nsGtkKeyUtils.cpp
+++ b/widget/gtk/nsGtkKeyUtils.cpp
@@ -12,11 +12,13 @@
 #include <gdk/gdkkeysyms.h>
 #include <algorithm>
 #include <gdk/gdk.h>
-#include <gdk/gdkx.h>
 #include <dlfcn.h>
 #include <gdk/gdkkeysyms-compat.h>
-#include <X11/XKBlib.h>
-#include "X11UndefineNone.h"
+#ifdef MOZ_X11
+#  include <gdk/gdkx.h>
+#  include <X11/XKBlib.h>
+#  include "X11UndefineNone.h"
+#endif
 #include "IMContextWrapper.h"
 #include "WidgetUtils.h"
 #include "WidgetUtilsGtk.h"
@@ -51,7 +53,9 @@ LazyLogModule gKeymapWrapperLog("KeymapWrapperWidgets");
 
 KeymapWrapper* KeymapWrapper::sInstance = nullptr;
 guint KeymapWrapper::sLastRepeatableHardwareKeyCode = 0;
+#ifdef MOZ_X11
 Time KeymapWrapper::sLastRepeatableKeyTime = 0;
+#endif
 KeymapWrapper::RepeatState KeymapWrapper::sRepeatState =
     KeymapWrapper::NOT_PRESSED;
 
@@ -346,9 +350,11 @@ KeymapWrapper::KeymapWrapper()
 
   g_object_ref(mGdkKeymap);
 
+#ifdef MOZ_X11
   if (GdkIsX11Display()) {
     InitXKBExtension();
   }
+#endif
 
   Init();
 }
@@ -366,7 +372,9 @@ void KeymapWrapper::Init() {
   memset(mModifierMasks, 0, sizeof(mModifierMasks));
 
   if (GdkIsX11Display()) {
+#ifdef MOZ_X11
     InitBySystemSettingsX11();
+#endif
   }
 #ifdef MOZ_WAYLAND
   else {
@@ -374,7 +382,9 @@ void KeymapWrapper::Init() {
   }
 #endif
 
+#ifdef MOZ_X11
   gdk_window_add_filter(nullptr, FilterEvents, this);
+#endif
 
   MOZ_LOG(gKeymapWrapperLog, LogLevel::Info,
           ("%p Init, CapsLock=0x%X, NumLock=0x%X, "
@@ -387,6 +397,7 @@ void KeymapWrapper::Init() {
            GetModifierMask(SUPER), GetModifierMask(HYPER)));
 }
 
+#ifdef MOZ_X11
 void KeymapWrapper::InitXKBExtension() {
   PodZero(&mKeyboardState);
 
@@ -619,6 +630,7 @@ void KeymapWrapper::InitBySystemSettingsX11() {
   XFreeModifiermap(xmodmap);
   XFree(xkeymap);
 }
+#endif
 
 #ifdef MOZ_WAYLAND
 void KeymapWrapper::SetModifierMask(xkb_keymap* aKeymap,
@@ -776,6 +788,7 @@ void KeymapWrapper::InitBySystemSettingsWayland() {
 #endif
 
 KeymapWrapper::~KeymapWrapper() {
+#ifdef MOZ_X11
   gdk_window_remove_filter(nullptr, FilterEvents, this);
   if (mOnKeysChangedSignalHandle) {
     g_signal_handler_disconnect(mGdkKeymap, mOnKeysChangedSignalHandle);
@@ -783,10 +796,12 @@ KeymapWrapper::~KeymapWrapper() {
   if (mOnDirectionChangedSignalHandle) {
     g_signal_handler_disconnect(mGdkKeymap, mOnDirectionChangedSignalHandle);
   }
+#endif
   g_object_unref(mGdkKeymap);
   MOZ_LOG(gKeymapWrapperLog, LogLevel::Info, ("%p Destructor", this));
 }
 
+#ifdef MOZ_X11
 /* static */
 GdkFilterReturn KeymapWrapper::FilterEvents(GdkXEvent* aXEvent,
                                             GdkEvent* aGdkEvent,
@@ -900,6 +915,7 @@ GdkFilterReturn KeymapWrapper::FilterEvents(GdkXEvent* aXEvent,
 
   return GDK_FILTER_CONTINUE;
 }
+#endif
 
 static void ResetBidiKeyboard() {
   // Reset the bidi keyboard settings for the new GdkKeymap
@@ -1761,6 +1777,7 @@ void KeymapWrapper::InitKeyEvent(WidgetKeyboardEvent& aKeyEvent,
   // key release events, the result isn't what we want.
   guint modifierState = aGdkKeyEvent->state;
   GdkDisplay* gdkDisplay = gdk_display_get_default();
+#ifdef MOZ_X11
   if (aGdkKeyEvent->is_modifier && GdkIsX11Display(gdkDisplay)) {
     Display* display = gdk_x11_display_get_xdisplay(gdkDisplay);
     if (XEventsQueued(display, QueuedAfterReading)) {
@@ -1777,6 +1794,7 @@ void KeymapWrapper::InitKeyEvent(WidgetKeyboardEvent& aKeyEvent,
       }
     }
   }
+#endif
   InitInputEvent(aKeyEvent, modifierState);
 
   switch (aGdkKeyEvent->keyval) {
@@ -2024,11 +2042,15 @@ bool KeymapWrapper::IsLatinGroup(guint8 aGroup) {
 }
 
 bool KeymapWrapper::IsAutoRepeatableKey(guint aHardwareKeyCode) {
+#ifdef MOZ_X11
   uint8_t indexOfArray = aHardwareKeyCode / 8;
   MOZ_ASSERT(indexOfArray < ArrayLength(mKeyboardState.auto_repeats),
              "invalid index");
   char bitMask = 1 << (aHardwareKeyCode % 8);
   return (mKeyboardState.auto_repeats[indexOfArray] & bitMask) != 0;
+#else
+  return false;
+#endif
 }
 
 /* static */
diff --git a/widget/gtk/nsGtkKeyUtils.h b/widget/gtk/nsGtkKeyUtils.h
index 07d60c2..2434b57 100644
--- a/widget/gtk/nsGtkKeyUtils.h
+++ b/widget/gtk/nsGtkKeyUtils.h
@@ -13,7 +13,9 @@
 #include "nsTArray.h"
 
 #include <gdk/gdk.h>
-#include <X11/XKBlib.h>
+#ifdef MOZ_X11
+#  include <X11/XKBlib.h>
+#endif
 #ifdef MOZ_WAYLAND
 #  include <gdk/gdkwayland.h>
 #  include <xkbcommon/xkbcommon.h>
@@ -232,8 +234,10 @@ class KeymapWrapper {
    * Initializing methods.
    */
   void Init();
+#ifdef MOZ_X11
   void InitXKBExtension();
   void InitBySystemSettingsX11();
+#endif
 #ifdef MOZ_WAYLAND
   void InitBySystemSettingsWayland();
 #endif
@@ -309,6 +313,7 @@ class KeymapWrapper {
    */
   int mXKBBaseEventCode;
 
+#ifdef MOZ_X11
   /**
    * Only auto_repeats[] stores valid value.  If you need to use other
    * members, you need to listen notification events for them.
@@ -316,6 +321,7 @@ class KeymapWrapper {
    * InitXKBExtension().
    */
   XKeyboardState mKeyboardState;
+#endif
 
   /**
    * Pointer of the singleton instance.
@@ -326,7 +332,9 @@ class KeymapWrapper {
    * Auto key repeat management.
    */
   static guint sLastRepeatableHardwareKeyCode;
+#ifdef MOZ_X11
   static Time sLastRepeatableKeyTime;
+#endif
   enum RepeatState { NOT_PRESSED, FIRST_PRESS, REPEATING };
   static RepeatState sRepeatState;
 
@@ -432,6 +440,7 @@ class KeymapWrapper {
    */
   static uint32_t GetDOMKeyCodeFromKeyPairs(guint aGdkKeyval);
 
+#ifdef MOZ_X11
   /**
    * FilterEvents() listens all events on all our windows.
    * Be careful, this may make damage to performance if you add expensive
@@ -439,6 +448,7 @@ class KeymapWrapper {
    */
   static GdkFilterReturn FilterEvents(GdkXEvent* aXEvent, GdkEvent* aGdkEvent,
                                       gpointer aData);
+#endif
 
   /**
    * MaybeDispatchContextMenuEvent() may dispatch eContextMenu event if
diff --git a/widget/gtk/nsPrintDialogGTK.cpp b/widget/gtk/nsPrintDialogGTK.cpp
index 0050af3..b032cfe 100644
--- a/widget/gtk/nsPrintDialogGTK.cpp
+++ b/widget/gtk/nsPrintDialogGTK.cpp
@@ -26,7 +26,10 @@
 #include "nsIObserverService.h"
 
 // for gdk_x11_window_get_xid
-#include <gdk/gdkx.h>
+#include <gdk/gdk.h>
+#ifdef MOZ_X11
+#  include <gdk/gdkx.h>
+#endif
 #include <sys/types.h>
 #include <sys/stat.h>
 #include <fcntl.h>
@@ -526,6 +529,7 @@ static gboolean window_export_handle(GtkWindow* window,
                                      GtkWindowHandleExported callback,
                                      gpointer user_data) {
   if (GdkIsX11Display()) {
+#ifdef MOZ_X11
     GdkWindow* gdk_window = gtk_widget_get_window(GTK_WIDGET(window));
     char* handle_str;
     guint32 xid = (guint32)gdk_x11_window_get_xid(gdk_window);
@@ -534,6 +538,7 @@ static gboolean window_export_handle(GtkWindow* window,
     callback(window, handle_str, user_data);
     g_free(handle_str);
     return true;
+#endif
   }
 #ifdef MOZ_WAYLAND
   else if (GdkIsWaylandDisplay()) {
diff --git a/widget/gtk/nsUserIdleServiceGTK.cpp b/widget/gtk/nsUserIdleServiceGTK.cpp
index 884d52a..37fe8d1 100644
--- a/widget/gtk/nsUserIdleServiceGTK.cpp
+++ b/widget/gtk/nsUserIdleServiceGTK.cpp
@@ -17,6 +17,7 @@ using mozilla::LogLevel;
 
 static mozilla::LazyLogModule sIdleLog("nsIUserIdleService");
 
+#ifdef MOZ_X11
 typedef bool (*_XScreenSaverQueryExtension_fn)(Display* dpy, int* event_base,
                                                int* error_base);
 
@@ -25,12 +26,14 @@ typedef XScreenSaverInfo* (*_XScreenSaverAllocInfo_fn)(void);
 typedef void (*_XScreenSaverQueryInfo_fn)(Display* dpy, Drawable drw,
                                           XScreenSaverInfo* info);
 
-static bool sInitialized = false;
 static _XScreenSaverQueryExtension_fn _XSSQueryExtension = nullptr;
 static _XScreenSaverAllocInfo_fn _XSSAllocInfo = nullptr;
 static _XScreenSaverQueryInfo_fn _XSSQueryInfo = nullptr;
+#endif
+static bool sInitialized = false;
 
 static void Initialize() {
+#ifdef MOZ_X11
   if (!mozilla::widget::GdkIsX11Display()) {
     return;
   }
@@ -59,14 +62,21 @@ static void Initialize() {
     MOZ_LOG(sIdleLog, LogLevel::Warning, ("Failed to get XSSQueryInfo!\n"));
 
   sInitialized = true;
+#endif
 }
 
+#ifdef MOZ_X11
 nsUserIdleServiceGTK::nsUserIdleServiceGTK() : mXssInfo(nullptr) {
+#else
+nsUserIdleServiceGTK::nsUserIdleServiceGTK() {
+#endif
   Initialize();
 }
 
 nsUserIdleServiceGTK::~nsUserIdleServiceGTK() {
+#ifdef MOZ_X11
   if (mXssInfo) XFree(mXssInfo);
+#endif
 
 // It is not safe to unload libXScrnSaver until each display is closed because
 // the library registers callbacks through XESetCloseDisplay (Bug 397607).
@@ -80,6 +90,7 @@ nsUserIdleServiceGTK::~nsUserIdleServiceGTK() {
 }
 
 bool nsUserIdleServiceGTK::PollIdleTime(uint32_t* aIdleTime) {
+#ifdef MOZ_X11
   if (!sInitialized) {
     // For some reason, we could not find xscreensaver.
     return false;
@@ -109,6 +120,7 @@ bool nsUserIdleServiceGTK::PollIdleTime(uint32_t* aIdleTime) {
   }
   // If we get here, we couldn't get to XScreenSaver:
   MOZ_LOG(sIdleLog, LogLevel::Warning, ("XSSQueryExtension returned false!\n"));
+#endif
   return false;
 }
 
diff --git a/widget/gtk/nsUserIdleServiceGTK.h b/widget/gtk/nsUserIdleServiceGTK.h
index 9b9ba31..e596283 100644
--- a/widget/gtk/nsUserIdleServiceGTK.h
+++ b/widget/gtk/nsUserIdleServiceGTK.h
@@ -9,10 +9,13 @@
 #define nsUserIdleServiceGTK_h__
 
 #include "nsUserIdleService.h"
-#include <X11/Xlib.h>
-#include <X11/Xutil.h>
-#include <gdk/gdkx.h>
+#ifdef MOZ_X11
+#  include <X11/Xlib.h>
+#  include <X11/Xutil.h>
+#  include <gdk/gdkx.h>
+#endif
 
+#ifdef MOZ_X11
 typedef struct {
   Window window;               // Screen saver window
   int state;                   // ScreenSaver(Off,On,Disabled)
@@ -21,6 +24,7 @@ typedef struct {
   unsigned long idle;          // milliseconds idle
   unsigned long event_mask;    // event stuff
 } XScreenSaverInfo;
+#endif
 
 class nsUserIdleServiceGTK : public nsUserIdleService {
  public:
@@ -40,7 +44,9 @@ class nsUserIdleServiceGTK : public nsUserIdleService {
 
  private:
   ~nsUserIdleServiceGTK();
+#ifdef MOZ_X11
   XScreenSaverInfo* mXssInfo;
+#endif
 
  protected:
   nsUserIdleServiceGTK();
diff --git a/widget/gtk/nsWindow.cpp b/widget/gtk/nsWindow.cpp
index 7f77730..a57d051 100644
--- a/widget/gtk/nsWindow.cpp
+++ b/widget/gtk/nsWindow.cpp
@@ -58,7 +58,9 @@
 #include "mozilla/UniquePtrExtensions.h"
 #include "mozilla/WidgetUtils.h"
 #include "mozilla/WritingModes.h"
-#include "mozilla/X11Util.h"
+#ifdef MOZ_X11
+#  include "mozilla/X11Util.h"
+#endif
 #include "mozilla/XREAppData.h"
 #include "NativeKeyBindings.h"
 #include "nsAppDirectoryServiceDefs.h"
@@ -120,7 +122,9 @@ using namespace mozilla::gfx;
 using namespace mozilla::layers;
 using namespace mozilla::widget;
 using mozilla::gl::GLContextEGL;
+#ifdef MOZ_X11
 using mozilla::gl::GLContextGLX;
+#endif
 
 // Don't put more than this many rects in the dirty region, just fluff
 // out to the bounding-box if there are more
@@ -280,6 +284,7 @@ bool nsWindow::sTransparentMainWindow = false;
 
 namespace mozilla {
 
+#ifdef MOZ_X11
 class CurrentX11TimeGetter {
  public:
   explicit CurrentX11TimeGetter(GdkWindow* aWindow)
@@ -327,6 +332,7 @@ class CurrentX11TimeGetter {
   GdkWindow* mWindow;
   TimeStamp mAsyncUpdateStart;
 };
+#endif
 
 }  // namespace mozilla
 
@@ -2447,6 +2453,7 @@ static bool GetWindowManagerName(GdkWindow* gdk_window, nsACString& wmName) {
     return false;
   }
 
+#ifdef MOZ_X11
   Display* xdisplay = gdk_x11_get_default_xdisplay();
   GdkScreen* screen = gdk_window_get_screen(gdk_window);
   Window root_win = GDK_WINDOW_XID(gdk_screen_get_root_window(screen));
@@ -2510,6 +2517,7 @@ static bool GetWindowManagerName(GdkWindow* gdk_window, nsACString& wmName) {
 
   wmName = reinterpret_cast<const char*>(prop_return);
   return true;
+#endif
 }
 
 #define kDesktopMutterSchema "org.gnome.mutter"
@@ -2566,6 +2574,7 @@ void nsWindow::GetWorkspaceID(nsAString& workspaceID) {
   if (!GdkIsX11Display() || !mShell) {
     return;
   }
+#ifdef MOZ_X11
   // Get the gdk window for this widget.
   GdkWindow* gdk_window = gtk_widget_get_window(mShell);
   if (!gdk_window) {
@@ -2594,6 +2603,7 @@ void nsWindow::GetWorkspaceID(nsAString& workspaceID) {
 
   workspaceID.AppendInt((int32_t)wm_desktop[0]);
   g_free(wm_desktop);
+#endif
 }
 
 void nsWindow::MoveToWorkspace(const nsAString& workspaceIDStr) {
@@ -2603,6 +2613,7 @@ void nsWindow::MoveToWorkspace(const nsAString& workspaceIDStr) {
     return;
   }
 
+#ifdef MOZ_X11
   // Get the gdk window for this widget.
   GdkWindow* gdk_window = gtk_widget_get_window(mShell);
   if (!gdk_window) {
@@ -2636,6 +2647,7 @@ void nsWindow::MoveToWorkspace(const nsAString& workspaceIDStr) {
              SubstructureNotifyMask | SubstructureRedirectMask, &xevent);
 
   XFlush(xdisplay);
+#endif
 }
 
 using SetUserTimeFunc = void (*)(GdkWindow*, guint32);
@@ -2675,10 +2687,14 @@ guint32 nsWindow::GetLastUserInputTime() {
   // WM_DELETE_WINDOW delete events, but not usually mouse motion nor
   // button and key releases.  Therefore use the most recent of
   // gdk_x11_display_get_user_time and the last time that we have seen.
+#ifdef MOZ_X11
   GdkDisplay* gdkDisplay = gdk_display_get_default();
   guint32 timestamp = GdkIsX11Display(gdkDisplay)
                           ? gdk_x11_display_get_user_time(gdkDisplay)
                           : gtk_get_current_event_time();
+#else
+  guint32 timestamp = gtk_get_current_event_time();
+#endif
 
   if (sLastUserInputTime != GDK_CURRENT_TIME &&
       TimestampIsNewerThan(sLastUserInputTime, timestamp)) {
@@ -2737,6 +2753,7 @@ void nsWindow::SetFocus(Raise aRaise, mozilla::dom::CallerType aCallerType) {
         return;
       }
 
+#ifdef MOZ_X11
       uint32_t timestamp = GDK_CURRENT_TIME;
 
       nsGTKToolkit* GTKToolkit = nsGTKToolkit::GetToolkit();
@@ -2748,6 +2765,7 @@ void nsWindow::SetFocus(Raise aRaise, mozilla::dom::CallerType aCallerType) {
       gtk_window_present_with_time(GTK_WINDOW(owningWindow->mShell), timestamp);
 
       if (GTKToolkit) GTKToolkit->SetFocusTimestamp(0);
+#endif
     }
     return;
   }
@@ -2830,6 +2848,7 @@ void nsWindow::UpdateClientOffsetFromFrameExtents() {
     return;
   }
 
+#ifdef MOZ_X11
   if (!mIsTopLevel || !mShell ||
       gtk_window_get_window_type(GTK_WINDOW(mShell)) == GTK_WINDOW_POPUP) {
     mClientOffset = nsIntPoint(0, 0);
@@ -2869,6 +2888,7 @@ void nsWindow::UpdateClientOffsetFromFrameExtents() {
 
   LOG(("nsWindow::UpdateClientOffsetFromFrameExtents [%p] %d,%d\n", (void*)this,
        mClientOffset.x, mClientOffset.y));
+#endif
 }
 
 LayoutDeviceIntPoint nsWindow::GetClientOffset() {
@@ -2884,9 +2904,11 @@ gboolean nsWindow::OnPropertyNotifyEvent(GtkWidget* aWidget,
     return FALSE;
   }
 
+#ifdef MOZ_X11
   if (GetCurrentTimeGetter()->PropertyNotifyHandler(aWidget, aEvent)) {
     return TRUE;
   }
+#endif
 
   return FALSE;
 }
@@ -3025,9 +3047,11 @@ void* nsWindow::GetNativeData(uint32_t aDataType) {
       return GetToplevelWidget();
 
     case NS_NATIVE_WINDOW_WEBRTC_DEVICE_ID:
+#ifdef MOZ_X11
       if (GdkIsX11Display()) {
         return (void*)GDK_WINDOW_XID(gdk_window_get_toplevel(mGdkWindow));
       }
+#endif
       NS_WARNING(
           "nsWindow::GetNativeData(): NS_NATIVE_WINDOW_WEBRTC_DEVICE_ID is not "
           "handled on Wayland!");
@@ -3047,9 +3071,11 @@ void* nsWindow::GetNativeData(uint32_t aDataType) {
     case NS_NATIVE_OPENGL_CONTEXT:
       return nullptr;
     case NS_NATIVE_EGL_WINDOW: {
+#ifdef MOZ_X11
       if (GdkIsX11Display()) {
         return mGdkWindow ? (void*)GDK_WINDOW_XID(mGdkWindow) : nullptr;
       }
+#endif
 #ifdef MOZ_WAYLAND
       if (mContainer) {
         return moz_container_wayland_get_egl_window(mContainer,
@@ -3369,6 +3395,7 @@ gboolean nsWindow::OnExposeEvent(cairo_t* cr) {
   }
 #endif
 
+#ifdef MOZ_X11
   nsIWidgetListener* listener = GetListener();
   if (!listener) return FALSE;
 
@@ -3587,6 +3614,7 @@ gboolean nsWindow::OnExposeEvent(cairo_t* cr) {
 
   // check the return value!
   return TRUE;
+#endif
 }
 
 void nsWindow::UpdateAlpha(SourceSurface* aSourceSurface,
@@ -3906,6 +3934,7 @@ void nsWindow::OnMotionNotifyEvent(GdkEventMotion* aEvent) {
     MOZ_ASSERT(gdk_window, "gdk_window_get_toplevel should not return null");
 
     bool canDrag = true;
+#ifdef MOZ_X11
     if (GdkIsX11Display()) {
       // Workaround for https://bugzilla.gnome.org/show_bug.cgi?id=789054
       // To avoid crashes disable double-click on WM without _NET_WM_MOVERESIZE.
@@ -3916,6 +3945,7 @@ void nsWindow::OnMotionNotifyEvent(GdkEventMotion* aEvent) {
         canDrag = false;
       }
     }
+#endif
 
     if (canDrag) {
       gdk_window_begin_move_drag(gdk_window, 1, aEvent->x_root, aEvent->y_root,
@@ -4409,15 +4439,18 @@ TimeStamp nsWindow::GetEventTimeStamp(guint32 aEventTime) {
         BaseTimeDurationPlatformUtils::TicksFromMilliseconds(timestampTime);
     eventTimeStamp = TimeStamp::FromSystemTime(tick);
   } else {
+#ifdef MOZ_X11
     CurrentX11TimeGetter* getCurrentTime = GetCurrentTimeGetter();
     MOZ_ASSERT(getCurrentTime,
                "Null current time getter despite having a window");
     eventTimeStamp =
         TimeConverter().GetTimeStampFromSystemTime(aEventTime, *getCurrentTime);
+#endif
   }
   return eventTimeStamp;
 }
 
+#ifdef MOZ_X11
 mozilla::CurrentX11TimeGetter* nsWindow::GetCurrentTimeGetter() {
   MOZ_ASSERT(mGdkWindow, "Expected mGdkWindow to be set");
   if (MOZ_UNLIKELY(!mCurrentTimeGetter)) {
@@ -4425,6 +4458,7 @@ mozilla::CurrentX11TimeGetter* nsWindow::GetCurrentTimeGetter() {
   }
   return mCurrentTimeGetter.get();
 }
+#endif
 
 gboolean nsWindow::OnKeyPressEvent(GdkEventKey* aEvent) {
   LOG(("OnKeyPressEvent [%p]\n", (void*)this));
@@ -4594,6 +4628,7 @@ void nsWindow::OnWindowStateEvent(GtkWidget* aWidget,
   //
   // This may be fixed in Gtk 3.24+ but some DE still have this issue
   // (Bug 1624199) so let's remove it for Wayland only.
+#ifdef MOZ_X11
   if (GdkIsX11Display()) {
     if (!mIsShown) {
       aEvent->changed_mask = static_cast<GdkWindowState>(
@@ -4604,6 +4639,7 @@ void nsWindow::OnWindowStateEvent(GtkWidget* aWidget,
           aEvent->changed_mask | GDK_WINDOW_STATE_MAXIMIZED);
     }
   }
+#endif
 
   // This is a workaround for https://gitlab.gnome.org/GNOME/gtk/issues/1395
   // Gtk+ controls window active appearance by window-state-event signal.
@@ -5005,6 +5041,7 @@ static GdkWindow* CreateGdkWindow(GdkWindow* parent, GtkWidget* widget) {
   return window;
 }
 
+#ifdef MOZ_X11
 // Configure GL visual on X11. We add alpha silently
 // if we use WebRender to workaround NVIDIA specific Bug 1663273.
 bool nsWindow::ConfigureX11GLVisual(bool aUseAlpha) {
@@ -5061,6 +5098,7 @@ bool nsWindow::ConfigureX11GLVisual(bool aUseAlpha) {
 
   return true;
 }
+#endif
 
 nsCString nsWindow::GetWindowNodeName() {
   nsCString nodeName("Unknown");
@@ -5646,9 +5684,14 @@ nsresult nsWindow::Create(nsIWidget* aParent, nsNativeWidget aNativeParent,
        mWindowType == eWindowType_toplevel ? "Toplevel" : "Popup",
        mIsPIPWindow ? "PIP window" : ""));
   if (mShell) {
+#ifdef MOZ_X11
     LOG(("\tmShell %p mContainer %p mGdkWindow %p 0x%lx\n", mShell, mContainer,
          mGdkWindow,
          GdkIsX11Display() ? gdk_x11_window_get_xid(mGdkWindow) : 0));
+#else
+    LOG(("\tmShell %p mContainer %p mGdkWindow %p 0x%lx\n", mShell, mContainer,
+         mGdkWindow, 0));
+#endif
   } else if (mContainer) {
     LOG(("\tmContainer %p mGdkWindow %p\n", mContainer, mGdkWindow));
   } else if (mGdkWindow) {
@@ -6624,6 +6667,7 @@ void nsWindow::UpdateTitlebarTransparencyBitmap() {
     cairo_surface_destroy(surface);
   }
 
+#ifdef MOZ_X11
   if (!mNeedsShow) {
     Display* xDisplay = GDK_WINDOW_XDISPLAY(mGdkWindow);
     Window xDrawable = GDK_WINDOW_XID(mGdkWindow);
@@ -6646,6 +6690,7 @@ void nsWindow::UpdateTitlebarTransparencyBitmap() {
 
     XFreePixmap(xDisplay, maskPixmap);
   }
+#endif
 }
 
 void nsWindow::GrabPointer(guint32 aTime) {
@@ -6674,6 +6719,7 @@ void nsWindow::GrabPointer(guint32 aTime) {
     return;
   }
 
+#ifdef MOZ_X11
   gint retval;
   // Note that we need GDK_TOUCH_MASK below to work around a GDK/X11 bug that
   // causes touch events that would normally be received by this client on
@@ -6700,6 +6746,7 @@ void nsWindow::GrabPointer(guint32 aTime) {
                           &nsWindow::CheckForRollupDuringGrab);
     NS_DispatchToCurrentThread(event.forget());
   }
+#endif
 }
 
 void nsWindow::ReleaseGrabs(void) {
@@ -6713,7 +6760,9 @@ void nsWindow::ReleaseGrabs(void) {
     return;
   }
 
+#ifdef MOZ_X11
   gdk_pointer_ungrab(GDK_CURRENT_TIME);
+#endif
 }
 
 GtkWidget* nsWindow::GetToplevelWidget() {
@@ -8203,6 +8252,7 @@ bool nsWindow::GetDragInfo(WidgetMouseEvent* aMouseEvent, GdkWindow** aWindow,
     return false;
   }
 
+#ifdef MOZ_X11
   if (GdkIsX11Display()) {
     // Workaround for https://bugzilla.gnome.org/show_bug.cgi?id=789054
     // To avoid crashes disable double-click on WM without _NET_WM_MOVERESIZE.
@@ -8218,6 +8268,7 @@ bool nsWindow::GetDragInfo(WidgetMouseEvent* aMouseEvent, GdkWindow** aWindow,
       }
     }
   }
+#endif
 
   // FIXME: It would be nice to have the widget position at the time
   // of the event, but it's relatively unlikely that the widget has
@@ -8945,6 +8996,7 @@ void nsWindow::GetCompositorWidgetInitData(
     mozilla::widget::CompositorWidgetInitData* aInitData) {
   nsCString displayName;
 
+#ifdef MOZ_X11
   if (GdkIsX11Display() && mXWindow != X11None) {
     // Make sure the window XID is propagated to X server, we can fail otherwise
     // in GPU process (Bug 1401634).
@@ -8952,12 +9004,19 @@ void nsWindow::GetCompositorWidgetInitData(
     XFlush(display);
     displayName = nsCString(XDisplayString(display));
   }
+#endif
 
   bool isShaped =
       mIsTransparent && !mHasAlphaVisual && !mTransparencyBitmapForTitlebar;
+#ifdef MOZ_X11
   *aInitData = mozilla::widget::GtkCompositorWidgetInitData(
       (mXWindow != X11None) ? mXWindow : (uintptr_t) nullptr, displayName,
       isShaped, GdkIsX11Display(), GetClientSize());
+#else
+  *aInitData = mozilla::widget::GtkCompositorWidgetInitData(
+      (uintptr_t) nullptr, displayName,
+      isShaped, GdkIsX11Display(), GetClientSize());
+#endif
 }
 
 #ifdef MOZ_WAYLAND
diff --git a/widget/gtk/nsWindow.h b/widget/gtk/nsWindow.h
index 8799445..a9185ac 100644
--- a/widget/gtk/nsWindow.h
+++ b/widget/gtk/nsWindow.h
@@ -93,7 +93,9 @@ typedef struct _GdkEventTouchpadPinch GdkEventTouchpadPinch;
 
 namespace mozilla {
 class TimeStamp;
+#ifdef MOZ_X11
 class CurrentX11TimeGetter;
+#endif
 
 }  // namespace mozilla
 
@@ -274,7 +276,9 @@ class nsWindow final : public nsBaseWidget {
 
   WidgetEventTime GetWidgetEventTime(guint32 aEventTime);
   mozilla::TimeStamp GetEventTimeStamp(guint32 aEventTime);
+#ifdef MOZ_X11
   mozilla::CurrentX11TimeGetter* GetCurrentTimeGetter();
+#endif
 
   virtual void SetInputContext(const InputContext& aContext,
                                const InputContextAction& aAction) override;
@@ -782,7 +786,9 @@ class nsWindow final : public nsBaseWidget {
    */
   RefPtr<mozilla::widget::IMContextWrapper> mIMContext;
 
+#ifdef MOZ_X11
   mozilla::UniquePtr<mozilla::CurrentX11TimeGetter> mCurrentTimeGetter;
+#endif
   static GtkWindowDecoration sGtkWindowDecoration;
 
   static bool sTransparentMainWindow;
